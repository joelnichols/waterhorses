<h2>Support</h2>

<p>Support er til for at hjælpe dig. Support-siden er delt ind i 3 kategorier: <br>
- Til forældre<br>
- Regler<br>
- FAQ <br></p>
<hr>
<p><b>Til forældre</b><br>
Waterhorses.DK er en side udviklet af Camilla Gonzalez og Mikkel Larsen. Det er en side udviklet som et spil for børn, unge og voksne.<br>
Siden er uforpligtende og gratis. Man vil på et tidspunkt kunne købe VIP og WKR for <I>rigtige danske kroner,</i>
men dette vil foregå gennem et sikkert system.<br>
Vi vil undgå at brugere kan betale med f.eks SMS, for at være sikker på, at dit barn ikke køber noget på vores side, uden at
få lov. <br>
<br>
Hvis du gerne vil snakke personligt med administratorene fra siden, kan du skrive til os på mail <a href="mailto:admin@waterhorses.dk" target="_blank"><i>admin@waterhorses.dk</i></a><br>
Vi besvarer gerne alle jeres spørgsmål, for at sikre os, at dit barn eller teenager færdes sikkert på internettet.</p>
<hr>
<p><b>Regler</b><br>
Der er et regelsæt som alle brugere her på siden skal følge. Overtrædelse af reglerne kan medføre karantæne
og i værste tilfælde banlysning fra siden.
<br>
<br><u> $1 - Opførsel </u>
<br>$1,1 Det er vigtigt at der er plads til alle og I skal derfor tale pænt til hinanden.
<br>$1,2 Rasisme, pornografiske illustrationer, links mm. er <b>ikke</b> tilladt.
<br>$1,3 Du skal lytte til moderatore og administratorene, da det er dem, der håndhæver reglerne.
<br>
<br><u> $2 - Spillet </u>
<br>$2,1 Det er vigtigt at spillet er sjovt for alle. Snyd, bedrag og hacking er derfor <b>strengt</b> forbudt.
<br>$2,2 Hacking resulterer til omgående udelukning fra siden - uden varsel.
<br>$2,3 Alle situationer bliver håndteret af enten en moderator eller administrator, alle sager bliver derfor vurderet individuelt.
<br>$2,4 Spil fair, så er det sjovest for alle.
<br>
<br><u> $3 - Hvem skriver jeg til? </u>
<br>$ 3,1 Hvis du er kommet ud for en dårlig situation med hensyn til spillet, skriver du til en administrator (Foxystar) eller moderator. 
<br>$ 3,2 Hvis du har opdaget en teknisk fejl, skriv straks til Foxystar.
<br>$ 3,3 Forslag til siden mm. skal <i>ikke</i> sendes privat til hverken administratorer eller moderatore, hvis du har et forslag er du velkommen til at skrive i Hyggesnak, vi tjekker emnerne igennem flere gange dagligt.
</p>
<hr>
<p><b>FAQ</b></p>
<p><u>Generelt</u><br>
<ul>
<li>Hvor mange brugere må jeg eje?</li>
<li>Jeg er på samme IP som mine søskende, hvad gør jeg?</li>
<li>Hvorfor kan jeg ikke logge ind?</li>
<li>Hvordan sletter jeg min bruger?</li>
<li>Må vi være flere på én bruger?</li>
<li>Jeg har glemt min kode!</li>
<li>Jeg har fået ny email</li>
<li>Hvem er moderator og administrator, og hvordan kan jeg kende dem? <li>
<li>Forskellen på admin, SMOD og MOD <li>
<li>Forkortelser <li>
<li>Hvad er VIP? (IKKE AKTUELT!!)<li>
</ul>
</p>
<p><u>Om spillet</u><br>
<ul>
<li>Hvordan køber jeg en hest/WH?</li>
<li>Hvordan bliver jeg VHT/Moderator?</li>
<li>Hvordan tjener jeg wkr?</li>
<li>Kan man købe wkr?</li>
<li>Kan jeg skifte mit brugernavn? <li>
<li>Hvordan kan jeg se, om jeg har fået en besked?<li>
<li>Hvornår stiger min hest i level?<li>
</ul>
<p><u>Sikkerhed</u><br>
<ul>
<li>Jeg tror jeg er blevet hacket, hvad gør jeg?</li>
<li>Må jeg give mit password ud?</li>
<li>Skal jeg have lov af mine forældre?</li>
</ul>
</p>
<p><u>Teknik</u><br>
<ul>
<li>Hvordan laver jeg en automatisk side?</li>
<li>Kan I lave en automatisk side for mig?</li>
</ul>
</p>
<hr>
<p><b>FAQ - GENERELT</B><BR>
<p><u>Hvor mange brugere må jeg eje?</u><br>
<ul>
	<li>Du må eje én bruger. Reglen hedder én bruger pr. person - vi synes at spillet skal foregå på
	fair vis, og hvis én person render rundt med 3-4 brugere, så vil denne person hurtigt få en kæmpe
	fordel med hensyn til wkr og heste. <i>Det er derfor ikke tilladt at have mere end én bruger</i><br>
	Vi har IP-logs over alle logins, og kan derved holde øje. Alle anmeldeser om snyd på denne vis vil blive
	taget meget alvorligt.<br>
	<i>Hvis du har mistanke til at en bruger har flere brugere, kontakt da en moderator eller admin, så vil vi 
	undersøge det.</i></li></p>
</ul>
<p><u>Jeg er på samme IP som mine søskende, hvad gør jeg?</u><br>
<ul>
	<li>Vi banlyser ikke folk uden varsel, bare fordi de er på samme IP. Du kan skrive en besked til en moderator
	eller admin, at du altså har søskende på siden, og I er på samme IP.<br>
	Derfra vil din sag blive behandlet alt efter om vi har brug for beviser eller ej.
	</li>
</ul>
</p>
<p><u>Hvorfor kan jeg ikke logge ind?</u><br>
<ul>
	<li>Du skal huske at du skal bruge din e-mail til at logge ind med, og ikke dit brugernavn.<br>
	Hvis dette stadig ikke virker, kan du skrive til os på mail <a href="mailto:admin@waterhorses.dk" target="_blank"><i>admin@waterhorses.dk</i></a>
	</li>
</ul>
</p>
<p><u>Hvordan sletter jeg min bruger?</u><br>
<ul>
	<li>For at slette din bruger skal du skrive en e-mail til os. Vi vil derefter bede om diverse beviser på, at du
	er ejeren af brugeren.
	</li>
</ul>
</p>
<p><u>Må vi være flere på én bruger?</u><br>
<ul>
	<li>Nej, det hedder én bruger pr. person, hverken mere eller mindre.<br>
	For at undgå uretfærdigheder, så er "samarbejdsstutterier" derfor ikke tilladt. I kan dog sagtens have et
	tæt samarbejde fra hver sin bruger - det blander vi os ikke i.
	</li>
</ul>
</p>
<p><u>Jeg har glemt min kode!</u><br>
<ul>
	<li>Skriv en e-mail til os, så tilsender vi en ny kode til den email tilknyttet din konto. 
	</li>
</ul>
</p>
<p><u>Jeg har fået ny email</u><br>
<ul>
	<li>Log ind på din konto og ændré din email under "Ret profil", så er det hele i orden igen.
	</li>
</ul>
</p>
<p><u>Hvem er moderator og administrator og hvordan kan jeg kende dem?</u><br>
<ul>
	<li>Du kan finde en liste på moderatore og administratore på forummet under "Spørgsmål og hjælp."<br>
	Du kan kende dem på deres farvede navn.<br>
	<I>Farveoversigt:</i><br>
	Administrator: <font color="#FF0000">Rødt navn</font><br>
	Supermoderator: <font color="#0000FF">Blåt navn</font><br>
	Moderator: <font color="#008000">Grønt navn</font><br>
	VHT/Vandhestetegner: <font color="#FFA500">Gult navn</font><br>
	VIP: <font color="#FF00FF">Pink navn</font><br>
	</li>
</ul>
</p>
<p><u>Forskellen på admin, SMOD og MOD</u><br>
<ul>
	<li>Der er tre forskellige titler blandt holdet bag Waterhorses.<br>
	Den første er admin - dette er grundlæggerne af siden og "overhovederne" - det er os, der bestemmer
	over siden og styrer den.<br>
	Så er der SMOD / Supermoderator, dette er admins højre hånd. De tager sig af større sager og problemer
	til tider med hjælp fra admin hvis dette er nødvendigt.<br>
	Til sidst har vi MOD / Moderator, det er dem der håndterer mindre problemer, så som skænderier på foraerne,
	misforståelser blandt brugerne og lignende.<br>
	<br>
	Der skal ydes lige meget respekt for alle tre parter - de gør alle et stort stykke arbejde for siden.<br>
	</li>
</ul>
</p>
<p><u>Forkotelser</u><br>
<ul>
	<li>WH - Waterhorse (vandhest)<br>
	WHS - WH Shoppen<br>
	WHM - WH Markedet <br>
	PM - Private message (besked)<br>
	Admin - Administrator<br>
	SMOD - Supermoderator<br>
	MOD - Moderator<br>
	VHT - Vandhestetegner<br>
	</li>
</ul>
</p>
<hr>
<p><b>FAQ - OM SPILLET</B><BR>
<p><u>Hvordan køber jeg en WH?</u><br>
<ul>
	<li>Du kan købe en WH fra "WH Shoppen" eller fra "WH Markedet". På WH Markedet sælger brugere privat til andre brugere,
	de kan sætte deres heste til salg tilgængeligt for alle, eller reserveret til en bestemt spiller.
	</li>
</ul>
</p>
<p><u>Hvordan bliver jeg VHT/MOD?</u><br>
<ul>
	<li>Du kan ikke ansøge om at blive VHT eller MOD. Det er Admin (Foxystar), der afgører, hvem der bliver VHT,
	SMOD og MOD på siden. Alle ansøgninger vil blive ignoreret.<br>
	Der er til tider VHT-konkurrencer, hvis vi står og mangler VHT'er. Dér kan man så deltage, og blive valgt hvis man
	er blandt de bedste. 
	</li>
</ul>
</p>
<p><u>Hvordan tjener jeg wkr?</u><br>
<ul>
	<li>Du kan tjene wkr (webkroner - virtuelle penge) på mange måder.<br>
	Du kan købe en dårlig hest, træne den op og derefter sælge den.<br>
	Du kan deltage i stævner med dine heste og tjene wkr den vej, hvis din hest bliver placeret i top 3.
	</li>
</ul>
</p>
<p><u>Kan man købe wkr?</u><br>
<ul>
	<li>Det er pt. ikke muligt at købe wkr, nej.</li>
</ul>
</p>
<p><u>Kan jeg skifte mit brugernavn?</u><br>
<ul>
	<li>Du kan skifte dit brugernavn ved at skrive til Foxystar.</li>
</ul>
</p>
<p><u>Hvordan kan jeg se, om jeg har fået en besked?</u><br>
<ul>
	<li>Alle ulæste beskeder er under "Oversigt" og "Indbakke" markeret med fed skrift.<br>
	Oppe i toppen vil der også stå, hvis du har ulæste beskeder.</li>
</ul>
</p>
<p><u>Hvornår stiger min hest i level?</u><br>
<ul>
	<li>Du kan se på processbaren under "Heste" hvornår din hest stiger i level.</li>
</ul>
</p>
</p>
<hr>
<p><b>FAQ - SIKKERHED</B><BR>
<p><u>Jeg tror jeg er blevet hacket, hvad gør jeg?</u><br>
<ul>
	<li>Skriv til en SMOD/MOD, med argumenter og beviser for, hvorfor du tror du er blevet hacket.</li>
</ul>
</p>
<p><u>Må jeg give mit password ud?</u><br>
<ul>
	<li>Giv aldrig dit password ud, til folk du ikke stoler på. Det kan være en idé at give dit password
	til dine forældre, i tilfælde af at du skulle glemme det.</li>
</ul>
</p>
<p><u>Skal jeg have lov af mine forældre?</u><br>
<ul>
	<li>Du skal ikke have lov af dine forældre for at lave en bruger, men det er en god idé at snakke med dine
	forældre før du begynder at færdes på internettet.</li>
</ul>
</p>
</p>
<hr>
<p><b>FAQ - TEKNIK</B><BR>
<p><u>Hvordan laver jeg en automatisk side?</u><br>
<ul>
	<li>Waterhorses er programmeret i HTML, PHP og MYSQL. Vi kan ikke forklare hvordan siden er lavet
	da det er et avanceret system.</li>
</ul>
</p>
<p><u>Kan I lave en automatisk side for mig?</u><br>
<ul>
	<li>Nej, det kan vi ikke. Det har vi hverken tid eller penge til. Det koster penge at lave sådan en side
	og det tager også rigtig meget tid.</li>
</ul>
</p>