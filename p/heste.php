<?php  # Ready for 2.3+ (Recode parts of this)

if(online()){

	echo '<h2>Vandheste</h2>';
	echo '<p>Her kan du se alle dine vandheste. Her har du også mulighed for at træne dem eller sælge dem.<br/>Du kan træne dine vandheste hver fjerde time.</p>
	<p>Klik på navnet for at redigere oplysningerne på din hest.</p><hr>';
	
	if(isset($_GET['train'])){
		
		if(getHorsesData($_GET['train'], "owner_id") == getId()){
			echo doTrain($_GET['train']);
		} else {
			echo 'Dette er ikke din hest!';	
		}
		echo '<hr>';
	}
	
	/* Sælg */
	if(isset($_POST["sell"])){
	
		echo ' <form method="post" action="">Er du sikker på at du vil sælge din hest? Du vil kun modtage 75% af hestens værdi <input type="hidden" name="sellc" value="'.$_POST["sell"].'"><button class="btn" type="submit">Ja!</button></form><hr>';

	}
	if(isset($_POST["sellc"])){
		
		$sc = safestrip($_POST["sellc"]);
		if(getHorsesData($sc,"owner_id")==getId()){
		
		
			$wkr = getUserData(getId(), "wkr");
			$value = getHorsesData($sc, "value");
			$value = $value*0.75; // Træk 25% fra
			$total = ($wkr+$value);
		
			$sql1 = mysql_query("UPDATE wh2_horses SET owner_id=0 WHERE id=".$sc);
			$sql2 = mysql_query("UPDATE wh2_users SET wkr=".$total." WHERE id=".getId());
			if($sql1 && $sql2){
				echo 'Du har solgt '.getHorsesData($sc,"name").' og modtaget '.$value.' wkr for den.<hr>';
			} else {
				echo "Du kan ikke sælge denne<hr>";
			}
	
		} else {
			echo 'Fejl, kontakt en administrator hvis dette fortsætter.<hr>';
		}
	}
	
	
	/* Vis dine vandheste */
	$sql = mysql_query("SELECT * FROM wh2_horses WHERE owner_id=".getId());
	while($row = mysql_fetch_array($sql)){
		$sql2 = mysql_query("SELECT * FROM wh2_horse_pic WHERE id=".$row['horse_id']);
		$row2 = mysql_fetch_array($sql2);
		
		echo '<div id="hest" style="display:inline-block;line-height:22px;width:100%;">';
		
		echo '<span style="float:left;width:50%;text-align: center;"><img src="./img/heste/'.$row2["src"].'" /></span>'; // left
		echo '<span style="float:left;width:50%;">'; // right
		echo '<b>ID:</b> '.$row['id'].'<br/>';
		echo '<b>Navn:</b> <a href="vishest-'.$row["id"].'"> '.$row['name'].'</a><br/>';
		echo '<b>Alder:</b> '.getHAge($row2['color']).'<br/>';
		
		echo '<b>Race:</b> '.$row2['race'].'<br/>';
		echo '<b>Farve:</b> '.$row2['color'].'<br/>';
		echo '<b>Køn:</b> '.getHSex($row['sex']).'<br/>';
		
		if($row['unik']==1){ $row['unik'] = "Ja"; } else { $row['unik'] = "Nej"; }
		echo '<b>Unik:</b> '.$row['unik'].'<br/>';
		echo '<b>Tegnet af:</b> '.getUserData($row2['designer_id'],"username").'<br/>';
		echo '<b>Værdi:</b> '.$row['value'].' wkr.<br/>';
		echo '<b>XP:</b> '. $row['xp'] .', <b>Level:</b> '. getXp2Level($row['xp']) .'<br/>';
		
		/* Progress bar to next level */
		$xp = $row['xp'];
		$level_xp = getLevel2Xp(getXp2Level($row['xp']));
		$next_xp = getLevel2Xp(getXp2Level($row['xp'])+1);
		$p = round(($xp-$level_xp)/($next_xp - $level_xp)*100);
		echo "<progress value='{$p}' max='100'></progress>";

		/* 3 buttons - Rediger, Træn, Sælg */
		echo '<div style="text-align:center;">
			<form style="display:inline;" action="./vishest-'.$row['id'].'" style="display:inline;"><button class="btn" style="width:32%" type="submit">Rediger</button></form>
			<form style="display:inline;" action="./heste-&train='.$row['id'].'" "><button class="btn" style="width:32%" type="submit">Træn</button></form>
			<form style="display:inline;" method="post" ><input type="hidden" name="sell" value="'.$row['id'].'"/><button class="btn" style="width:32%" type="submit">Sælg til WHS</button></form>
			</div>
		';

		echo '</span>';
		
		echo '</div>';
		echo '<hr/>';
	}
	if(mysql_num_rows($sql)==0){
		echo 'Du har ingen vandheste. Besøg WH Shoppen.';
	}
	
}

?>