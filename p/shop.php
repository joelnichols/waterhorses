<?php # Ready for 2.3+
if(online()){
	echo '<h2>WH Shoppen</h2>';
	echo '<p>Her kan du købe vandheste.</p><hr>';

	/* Køb funktionen */

	if(isset($_POST["id"]) && isForSale($_POST["id"])){
		
		$wkr = getUserData(getId(), "wkr");
		$price = getHorsesData($_POST["id"], "value");
		
		$total = ($wkr-$price);
		
		$lt = time();
		if($wkr-$price<0){ echo 'Du har ikke råd.<hr>'; } else {
			$sql1 = mysql_query("UPDATE wh2_horses SET owner_id=".getId().", last_train=".$lt." WHERE id=".$_POST["id"]);
			$sql2 = mysql_query("UPDATE wh2_users SET wkr=".$total." WHERE id=".getId());
			
			if($sql1 && $sql2){
				echo 'Du har købt '.getHorsesData($_POST["id"],"name").' for '.$price.' wkr.<hr>';
			} else {
				echo 'Fejl, kontakt en administrator hvis dette fortsætter.<hr/>';
			}
		}
	}


	/* Vis vandheste der er til salg */
	$sql = mysql_query("SELECT * FROM wh2_horses WHERE owner_id=0 ORDER BY value ASC");
	while($row = mysql_fetch_array($sql)){
		$sql2 = mysql_query("SELECT * FROM wh2_horse_pic WHERE id=".$row['horse_id']);
		$row2 = mysql_fetch_array($sql2);

		echo '<div id="hest" style="display:inline-block;width:100%;">';

		echo '<span style="text-align:center;float:left;width:50%;"><img src="./img/heste/'.$row2["src"].'" /></span>'; // Left
		echo '<span style="float:left;width:50%;line-height:22px;">'; // Right
		echo '<b>ID:</b> '.$row['id'].'<br/>';
		echo '<b>Navn:</b> <a href="vishest-'.$row["id"].'"> '.$row['name'].'</a><br/>';
		echo '<b>Alder:</b> '.getHAge($row2['born']).'<br/>';

		echo '<b>Race:</b> '.$row2['race'].'<br/>';
		echo '<b>Farve:</b> '.$row2['color'].'<br/>';
		echo '<b>Køn:</b> '.getHSex($row['sex']).'<br/>';

		if($row['unik']==1){ $row['unik'] = "Ja"; } else { $row['unik'] = "Nej"; }
		echo '<b>Unik:</b> '.$row['unik'].'<br/>';
		echo '<b>XP:</b> '. $row['xp'] .', <b>Level:</b> '. getXp2Level($row['xp']).'<br>';
		echo '<b>Værdi:</b> '.$row['value'].' wkr.<br/>';
		echo '<b>Tegnet af:</b> '.getUserData($row2['designer_id'],"username").'<br/>';

		/* Progress bar to next level */
		$xp = $row['xp'];
		$level_xp = getLevel2Xp(getXp2Level($row['xp']));
		$next_xp = getLevel2Xp(getXp2Level($row['xp'])+1);
		$p = round(($xp-$level_xp)/($next_xp - $level_xp)*100);
		echo "<progress value='{$p}' max='100'></progress>";

		/* 3 buttons - Rediger, Træn, Sælg */
		echo '<div style="text-align:center;">
			<form style="display:inline;" action="./vishest-'.$row['id'].'" style="display:inline;"><button class="btn" style="width:49%" type="submit">Vis hest</button></form>
			<form style="display:inline;" method="post" ><input type="hidden" name="id" value="'.$row['id'].'"/><button class="btn" style="width:49%" type="submit">Køb</button></form>
			</div>
		';


		echo '</span>';

		echo '</div>';
		echo '<hr/>';
	}
	if(mysql_num_rows($sql)==0){
		echo 'Der er desværre ingen heste til salg, prøv at kigge ind senere.';
	}
}
?>