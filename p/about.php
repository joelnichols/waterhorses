<h2>Forside</h2>

<p>Velkommen til Waterhorses.</p>

<p>Waterhorses er en automatisk vandheste side, hvilket vil sige at du får det hele lige med det samme, og ikke skal vente på at anden gør det for dig!<br>
Siden er for piger og drenge på alle aldre, og kun fantasien sætter grænser. På wh vil der også være mulighed for at snakke med andre hesteglade folk, eller bare i det hele taget snakke med andre jævnaldrende.</p>
<p>Har du det der skal til, for at opbygge et berømt og sucessfuldt stutteri nede på havets bund? </p>
<p>Du starter med 15000 wkr</p>
<hr>
<p>Du kan læse regler, FAQ og information om siden under <a href="support"><i>Support</i></a>. Husk at læse om alt dette, inden du laver din bruger.</p>

<p>Det er <b>gratis</b> at oprette en bruger og være medlem og du kan komme i gang lige med det samme!</p>

<p>Har du spørgsmål er du velkommen til at skrive til os på <a href="mailto:admin@waterhorses.dk" target="_blank"><i>admin@waterhorses.dk</i></a></p> 