<?php
require_once('chat_err.php');
require_once('chat_conf');

class Chat
{
	private $mysqli;
	
	// constructor open database connection
	function __construct(){
		$this->mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
		
	}
	
	// destructor closes database
	function __destruct(){
		$this->mysqli->close();
	}
	
	// Truncates (empties) the table containing all messages
	public function deleteAllMessages(){
		$sql = 'TRUNCATE TABLE wh2_chat';
		$result = $this->mysqli->query($sql);
	}
	
	// 
	public function postNewMessage($username, $message, $color){
		$username = $this->mysqli->real_escape_string($username);
		$message = $this->mysqli->real_escape_string($message);
		$color = $this->mysqli->real_escape_string($color);

		$sql = 'INSERT INTO wh2_chat (date, username, message, color)'.
		' VALUES (
		"'. time() .'",
		"'. $username .'",
		"'. $color .'" )';
		$result = $this->mysqli->query($sql);
	}
	
	// Get new messages
	public function getNewMessages($id=0){
		$id = $this->mysqli->real_escape_string($id);
		if($id>0)
		{
			$sql = 'SELECT message_id, username, message, color, date AS date FROM wh2_chat WHERE message_id>'. $id .' ORDER BY message_id ASC';
		}
		else
		{
			$sql = 'SELECT message_id, username, message, color, date FROM (SELECT message_id, username, message, color, date AS date FROM wh2_chat ORDER BY message_id DESC LIMIT 50) AS Last50 ORDER BY message_id ASC )';	
		}
		
		$result = $this->mysqli->query($sql);
		
		//XMP Response
		$response = '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>';
		$response .= '<response>';
		$response .= $this->isDatabaseCleared($id);
		
		if($result->num_rows){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$id = $row['message_id'];
				$color = $row['color'];
				$username = $row['username'];
				$time = $row['date'];
				$message = $row['message'];
				$response .= '<id>'. $id .'</id>' .
							 '<color>'. $color .'</color>' .
							 '<time>'. $time .'</time>' .
							 '<name>'. $name .'</name>' .
							 '<message>'. $message .'</message>';
			}
			$result->close();
		}
		
		$response .= '</response>';
		return $response;
		
	}
}
?>