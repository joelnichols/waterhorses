<?php

set_error_handler('ChatErrorHandler', E_ALL);

function ChatErrorHandler($number, $text, $file, $line){
	if(ob_get_length()) ob_clean();
	$errorMessage = 'Error: ' . $number . chr(10) .
					'Message: ' . $text . chr(10) .
					'File: ' . $file . chr(10) .
					'Line: ' . $line;	
	echo $errorMessage;
	exit;
}


?>