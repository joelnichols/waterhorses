﻿<?php

if(!online()){
	# Offline
	?>
		<ul>
			<li><a href="./">Forside</a></li>
			<li><a href="./signup">Opret</a></li>
			<li><a href="./support">Support</a></li>
		</ul>
	<?php
} else {
	# Online
	?>
		<ul>
			<li><a href="./oversigt">Oversigt</a></li>
			<li><a href="./top">Top 10</a></li>
			<li><a href="./forum">Forum</a></li>
			<li><a href="./search">Søg</a></li>
			<li><a href="./edit">Ret profil</a></li>
			<li><a href="./support">Support</a></li>
			<?php
				if(getUserData(getId(),'level')>=2){
					echo '<li><a href="./admin">Admin</a></li>';
				}
			?>
			<li><a href="./logout">Log af</a></li>

		</ul>
	<?php 
}

?>