<?php
ob_start();
function getId(){
	return $_SESSION['wh2_id'];
}
function getUserData($id, $data){
	$sql = mysql_query("SELECT `$data` FROM wh2_users WHERE `id`='$id'");
	if(mysql_num_rows($sql)==1){
		return mysql_result($sql,0);
	}
	return null;
}
function getHorsesData($id, $data){
	$sql = mysql_query("SELECT ".$data." FROM wh2_horses WHERE id=".$id);
	if(mysql_num_rows($sql)==1){
		return mysql_result($sql,0);	
	}
	return 0;
}
function getHSex($s){
	
	if($s==1){ return "Hingst"; }
	if($s==0){ return "Hoppe"; }
	return "Ukendt";
}
function getHIco($s){
	
	if($s==1){ return "♂"; }
	if($s==0){ return "♀"; }
	return "Ukendt";
}
function getHAge($a){
	
	return "Ukendt";
	
}

function online(){
	if(isset($_SESSION['wh2_id'])){
		return true;
	} else {
		return false;
	}
}
function notOn(){
	echo 'You are not online.';
	// header('Location: ./');
}
function realdate($date){
	return date('d/m-Y H:i:s', $date);
}
function getXp2Level($xp){
	$level = 0;
	if( $xp >= 0 ) $level = 1;
	if( $xp > 200 ) $level = 2;
	if( $xp > 566 ) $level = 3;
	if( $xp > 1386 ) $level = 4;
	if( $xp > 3200 ) $level = 5;
	if( $xp > 7155 ) $level = 6;
	if( $xp > 15677 ) $level = 7;
	if( $xp > 33866 ) $level = 8;
	if( $xp > 72408 ) $level = 9;
	if( $xp > 153600 ) $level = 10;
	if( $xp > 323817 ) $level = 11;

	return $level;
}

function getLevel2Xp($level){
	$xp = 0;
	if( $level == 1 ) $xp = 0;
	if( $level == 2 ) $xp = 200;
	if( $level == 3 ) $xp = 566;
	if( $level == 4 ) $xp = 1386;
	if( $level == 5 ) $xp = 3200;
	if( $level == 6 ) $xp = 7155;
	if( $level == 7 ) $xp = 15677;
	if( $level == 8 ) $xp = 33866;
	if( $level == 9 ) $xp = 72408;
	if( $level == 10 ) $xp = 153600;
	if( $level == 11 ) $xp = 323817;

	return $xp;
}

function isForSale($id){
	$sql = mysql_query("SELECT owner_id FROM wh2_horses WHERE id=".$id);
	
	if(mysql_num_rows($sql)==1 && mysql_result($sql, 0) == 0)
	{
		return true;
	}
	return false;
}
function doTrain($id){
	$waittime = 14400; // Sec. = 4 Timer imellem hver træning.
	$wkr_rate = 2;
	$xp_rate = 1; // For events - 1 = normal speed, 2 = 2x speed.
	$sql = mysql_query("SELECT name,xp,value,last_train FROM wh2_horses WHERE id=".$id);
	$row = mysql_fetch_array($sql);
	$level = getXp2Level($row['xp']);
	
	if(time()-$row['last_train']>$waittime){
	//if(true){ // DON'T UNCOMMENT THIS!!!
		$base = $level*100;
		
		$min = round(sqrt($base/10)*$level);
		$max = round(sqrt($base/1)*$level);

		$xp = rand( $min, $max )*$xp_rate;

		$wkr = rand( $min, $max )*$wkr_rate;

/*
		// Old calc.
		$xp = (rand(2,20)*($level/2)*$xp_rate);
		$wkr = (rand(120,520)*$wkr_rate*($level*0.5));
*/
		mysql_query("UPDATE wh2_horses SET xp=".($row['xp']+$xp).", value=".($row['value']+$wkr).", last_train=".time()." WHERE id=".$id);
		return $row['name']." er trænet og fik ". $xp ." XP. Værdien er øget  med ". $wkr ." wkr.";
	} else {
		return 'Du kan først træne '.$row['name'].' igen '.date('H:i:s',$row['last_train']+$waittime);
	}
}
function doSell($id){
	return null;	
}
function getCountOnlineUsers(){
	$time = time()-1800; // Hvor lang tid der går før brugeren ikke er online mere. 
	
	$sql = mysql_query("SELECT COUNT(*) FROM wh2_users WHERE `last_date`>'$time'");
	$result = mysql_result($sql,0);
	
	if($result == 1){
		return $result." bruger online";
	} else {
		return $result." brugere online";
	}
}
function email2id($email){
	$sql = mysql_query("SELECT `id` FROM wh2_users WHERE `email`='$email'");
	return mysql_result($sql,0);
}
/* Fix this someday 
function isBan($id){
	
	if(banned){
		$sql = mysql_query("SELECT * FROM wh2_bans WHERE `id`='$id'");
		return $reason;
	} else {
		return false;
	}
}
*/
function ssearchOwnerId($i){
	if($i==0){	
		return '<a href="./shop">Ingen</a>';
	} else {
		return '<a href="./user-'.$i.'">'. getUserData($i,"username") .'</a>';
	}
}
function getColor($id){
/* Farver
VIP		#FF00FF
VHT		#FFA500

MOD		#008000
SMOD	#0000FF
ADMIN	#FF0000
*/
	$sql = mysql_query("SELECT * FROM wh2_users WHERE `id`='$id'");
	$row = mysql_fetch_array($sql);
	
	$color="black";
	if($row['vip']==1){
		#vip - pink
		$color="#FF00FF";
	}
	if($row['vht']==1){
		#vht - orange
		$color="#FFA500";
	}
	if($row['level']==2){
		#mod - green
		$color="#008000";
	}
	if($row['level']==3){
		#smod - blue
		$color="#0000FF";
	}
	if($row['level']>=4){
		#admin - red
		$color="#FF0000";
	}
	return $color;
}

function login($email, $pass){
	$email = htmlspecialchars($email);
	$pass = hash('sha256', $pass);
	$date = date('d/m-Y H:i:s');
	$ip = $_SERVER['REMOTE_ADDR'];
	$wp = 0;
	$sql = mysql_query("SELECT `id` FROM wh2_users WHERE `email`='$email' AND `password`='$pass'")or die(mysql_error());
	if(mysql_num_rows($sql) == 1){
		$wp = 1;
		$row = mysql_fetch_array($sql);
		$_SESSION['wh2_id'] = $row[0];
	}

	
	/*if(isBan()){
		$wp = 2;
	}*/
	
	mysql_query("INSERT INTO wh2_loginlog SET `user_email`='$email', `time`='$date', `ip`='$ip', `status`='$wp' ")or die(mysql_error());
	
	header('Location: ./');
}

function safestrip($string){
       $string = strip_tags($string);
	   $string = stripslashes($string);
       $string = mysql_real_escape_string($string);
       $string=htmlspecialchars($string);
       return $string;
}

function showProfile($id){
	$id=safestrip($id);
	$sql = mysql_query("SELECT * FROM wh2_users WHERE `id`='$id'");
	
	if(mysql_num_rows($sql) == 1){
		$row = mysql_fetch_assoc($sql);
	
	//	echo "<div style='float:right;text-align:right;'><a href=''>Send besked</a></div>"; /* Fix this */
		if($id != getId()) { echo "<form action='./post-{$_GET['id']}' style='display:inline;float:right;'><button class='btn' type='submit'>Send besked</button></form>"; }
	
		
		$color="black";
		if($row['vip']==1){
			#vip - pink
			$color="#FF00FF";
		}
		if($row['vht']==1){
			#vht - orange
			$color="#FFA500";
		}
		if($row['level']==2){
			#mod - green
			$color="#008000";
		}
		if($row['level']==3){
			#smod - blue
			$color="#0000FF";
		}
		if($row['level']>=4){
			#admin - red
			$color="#FF0000";
		}
		echo "<p style='font-size:25px;margin-top:5px;color:".$color."'>".$row['username']."</p>";
		
		echo "<img src='./img/uploads/".$row['logo']."' style='float: left; margin-right: 10px; margin-left: 0; margin-top:-20px;'/>";
		
		echo "<b>Navn: </b>".$row['navn']."<br>";
		echo "<b>Alder: </b>".$row['alder']."<br>";
		echo '<b>Bopæl: </b>'.$row['home'];
		echo "<br><b>Wkr:</b> ".$row['wkr']."<br>";
		echo "Sidst online: ".realdate($row['last_date'])."<br>";
		echo "Tilmeldt: ".realdate($row['reg_date'])."<br>";
		echo "<br><br><br>";
		echo "<b>Profiltekst:</b><br>";
		$text=$row['beskrivelse'];
		$text=htmlspecialchars($text);
		$text=bbcode_format($text);
		$text=str_replace("
","<br>",$text);
		$text=str_replace("&#91;","[",$text);
		$text=str_replace("&#93;","]",$text);
		echo $text;
		echo "<br><br><hr>";
		showPHorses($id);

		} else { echo "Brugeren blev ikke fundet."; }
	
}
function showPHorses($id){
	
	$sql = mysql_query("SELECT * FROM wh2_horses WHERE owner_id=".$id." ORDER BY xp DESC");
	echo '
	<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#518A4A" >
		<tr>
			<th width="4%">#</th>
			<th>Vandhest</th>
			<th>Race</th>
			<th>Farve</th>
			<th>Køn</th>
			<th>Værdi</th>
		</tr>
	';
	
		$i = 1;
		while($row = mysql_fetch_array($sql)){
			$sql2 = mysql_query("SELECT * FROM wh2_horse_pic WHERE id=".$row['horse_id']);
		$row2 = mysql_fetch_array($sql2);
		$img = $row2['src'];
			echo '<tr>
					<td width="4%" style="text-align: right;">'.$i.'</td>
					<td width="29%" ><a class="popup" href="vishest-'.$row["id"].'">'.$row['name'].'<span><img src="/img/heste/'.$img.'"/></span></a></td>
					<td width="25%"style="text-align: center;">'. $row2['race'] .'</td>
					<td width="20%"style="text-align: center;">'. $row2['color'] .'</td>
					<td width="5%" style="text-align: center;">'.getHIco($row['sex']).'</td>
					<td width="18%" style="text-align: center;">'. $row['value'] .' wkr</td>
			     </tr>
			';
		$i++;
		}
	echo '
	</table>
	';
	
}
function bbcode_format($str) { /* For at style tekst i post, beskrivelser og forum */
    #$str = htmlentities($str);

    $simple_search = array(
                '/\[b\](.*?)\[\/b\]/is',
                '/\[i\](.*?)\[\/i\]/is',
                '/\[u\](.*?)\[\/u\]/is',
                '/\[url\](.*?)\[\/url\]/is',
                '/\[align\=(left|center|right)\](.*?)\[\/align\]/is',
                '/\[img\](.*?)\[\/img\]/is',
                '/\[size\=(.*?)\](.*?)\[\/size\]/is',
                '/\[color\=(.*?)\](.*?)\[\/color\]/is',
	);

    $simple_replace = array(
                '<b>$1</b>',
                '<i>$1</i>',
                '<u>$1</u>',
                '<a href="$1" rel="nofollow" title="$1">$1</a>',
                '<div style="text-align: $1;">$2</div>',
                '<img style="max-width: 560px;" src="$1" alt="" />',#added alt attribute for validation
                '<span style="font-size: $1;">$2</span>',
                '<span style="color: $1;">$2</span>',
                );

    // Do simple BBCode's
    $str = preg_replace($simple_search, $simple_replace, $str);
    return $str;
}
# Forum
function countTopics($id){
	$sql = mysql_query("SELECT COUNT(*) FROM wh2_fora_threads WHERE `fora_id`='$id' AND `deleted`='0'");
	return mysql_result($sql,0);
}
function countPosts($id){
	$sql = mysql_query("SELECT COUNT(*) From wh2_fora_posts AS p, wh2_fora_threads AS t WHERE p.thread_id=t.id AND t.deleted=0 AND t.fora_id='$id'");
	return mysql_result($sql, 0);
}
function threadClosed($id){
	$sql = mysql_query("SELECT `lock` FROM wh2_fora_threads WHERE `id`='$id'");
	return mysql_result($sql,0);
}
function threadSticky($id){
	$sql = mysql_query("SELECT `sticky` FROM wh2_fora_threads WHERE `id`='$id'");
	return mysql_result($sql,0);
}
function threadDeleted($id){
	$sql = mysql_query("SELECT `deleted` FROM wh2_fora_threads WHERE `id`='$id'");
	return mysql_result($sql,0);
}
function userForumAccess($id, $forumid){
	
	$sql = mysql_query("SELECT `vht`,`level` FROM wh2_users WHERE `id`='$id'");
	
	if(mysql_num_rows($sql)==1){
		$row = mysql_fetch_array($sql);
		
		$vht = $row['vht'];
		$level = $row['level'];
		
		if($forumid > 0 && $forumid < 20){ // Alle kan se forums med id under 20
			return 1;
		}
		if($forumid == 30){ // VHT Forum - KUN VHT OG ADMINS HAR ADGANG
			if($vht == 1 || $level == 4){
				return 1;
			} else {
				return 0;
			}
		}
		if($forumid == 50){ // Admin & Mods Forum
			if($level >= 2){
				return 1;
			} else {
				return 0;
			}
		}
		return 0;
	}
	return 0;
}
function countForumAnswer($threadId){
	$sql = mysql_query("SELECT COUNT(*) FROM `wh2_fora_posts` WHERE `thread_id`='$threadId'");
	return mysql_result($sql,0);
}
function foraNewThread($userId, $foraId){
	$userLevel= getUserData($userId,'level');
	
	if($foraId == 1 && $userLevel > 1){ /* Nyheder, kun mods+ kan oprette */
		return 1;
	}
	if($foraId > 1){
		return 1;
	}
	return 0;
}
?>