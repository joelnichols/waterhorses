<?php include_once('conf.php'); include_once("analyticstracking.php"); ?>
<?php include_once('ipc.php'); ?> 
<!DOCTYPE html>
<html>
	<head>
		<title>Waterhorses</title>
		<meta charset="UTF-8">
		<link href="img/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="style.css" rel="stylesheet"/>
		<link href="style-2-3.css" rel="stylesheet"/>
		<script src="clock.js"></script>
		<script>
		function openBBC()
			{
			myWindow=window.open('bbc.html','bbc koder','width=590,height=200');
			myWindow.
			myWindow.focus();
		}
		</script>
	</head>
	<body>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/da_DK/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		  }(document, 'script', 'facebook-jssdk'));</script>
		<div id="smalltexttop">
	
		<div id="online"><a href="./online"><?php echo getCountOnlineUsers(); ?> </a> - <?php echo date('H:i:s') ?> </div>
		</div>
		
		<div id="wrap">
			<div id="left">
				<nav><?php include('p/menu.php'); ?></nav>		
			</div>
			<div id="right"><nav><?php include('p/menu2.php'); ?></nav>	
			</div>
			<div id="mid">
				<header><?php include('p/login.php'); ?></header>
				<div id="content"><?php include('content.php'); ?></div>
			</div>	
		</div>
		<footer>Waterhorses v2.1 &copy; 2014<br><br><div class="fb-like" data-href="http://facebook.com/waterhorses/" data-send="false" data-layout="button_count" data-width="300" data-show-faces="false"></div></footer>
	</body>
</html>