<?php

$arr = array(
	"about",
	"signup",
	"support",
	
	"oversigt",
	"online",
	"stats",
	"top",

	"user",
	"edit",
	"editlogo",
	
	"shop",
	"heste",
	"vishest",
	
	"post",
	"indbakke",
	"read",
	"readsend",
	"sendte",
	"nybesked",
	
	"search",
	
	"forum",
	"thread",
	
	"chat",

	"logout",
	
	"admin",
	"vht"
);

if(isset($_GET['page']) && in_array($_GET['page'],$arr)){
	$page = $_GET['page'];
	include('p/'. $page .'.php');
} else {
	if(!online()){
		include('p/about.php');
	} else {
		include('p/oversigt.php');
	}
}

?>